package com.example.bruno.aulabd;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.SQLException;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bruno.aulabd.dao.DAO;
import com.example.bruno.aulabd.domain.Contato;

import org.w3c.dom.Text;

import java.util.zip.Inflater;

public class DetalhesActivity extends AppCompatActivity {

  private TextView tv_nome, tv_email, tv_telefone;
  private FloatingActionButton bt_call;
  private Contato contato;
  private DAO dao;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_detalhes);
    Log.d("DETALHES", "ONCREATE");
    setTitle("Detalhes Contato");

    dao = DAO.getInstance(this);

    configurarTela();
    recuperarDados();
  }

  private void configurarTela() {
    tv_nome = (TextView) findViewById(R.id.tv_nome);
    tv_email = (TextView) findViewById(R.id.tv_email);
    tv_telefone = (TextView) findViewById(R.id.tv_telefone);

    bt_call = (FloatingActionButton) findViewById(R.id.bt_call);

    bt_call.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        call();
      }
    });
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu_detalhes, menu);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();

    switch (id) {
      case R.id.menu_edit:
        atualizar();
        return true;
      case R.id.menu_delete:
        remover();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  protected void onRestart() {
    Log.d("DETALHES", "ONRESTART");
    super.onRestart();
    this.contato = dao.buscar(contato.getId());
    setDados();
  }

  private void recuperarDados() {
    Intent intent = getIntent();
    this.contato = (Contato) intent.getSerializableExtra("contato");

    setDados();
  }

  private void setDados() {
    tv_nome.setText(contato.getNome());
    tv_email.setText(contato.getEmail());
    tv_telefone.setText(contato.getTelefone());
  }

  private void call() {
    Intent intent = new Intent(Intent.ACTION_CALL);
    intent.setData(Uri.parse("tel:" + tv_telefone.getText().toString()));
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

    if (ContextCompat.checkSelfPermission(DetalhesActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
      ActivityCompat.requestPermissions(DetalhesActivity.this, new String[]{Manifest.permission.CALL_PHONE}, Integer.parseInt("123"));
    } else {
      startActivity(intent);
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    switch (requestCode) {

      case 123:
        if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
          call();
        } else {
          Toast.makeText(DetalhesActivity.this, "Permissão não concedida", Toast.LENGTH_LONG).show();
        }
        break;
      default:
        break;
    }
  }

  private void atualizar() {
    Intent intent = new Intent(DetalhesActivity.this, AtualizarActivity.class);
    intent.putExtra("contato", contato);
    startActivity(intent);
  }

  private void remover() {
    new AlertDialog.Builder(this)
        .setMessage("Deseja realmente excluir?")
        .setCancelable(true)
        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialogInterface, int i) {
            try {
              dao.remover(contato);
            } catch (SQLException e) {

            }
            DetalhesActivity.this.finish();
          }
        })
        .setNegativeButton("Não", null)
        .show();
  }

  @Override
  protected void onDestroy() {
    Log.d("DETALHES", "ONDESTROY");
    super.onDestroy();
  }
}
