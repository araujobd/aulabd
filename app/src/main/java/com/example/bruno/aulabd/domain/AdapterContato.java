package com.example.bruno.aulabd.domain;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.bruno.aulabd.R;

import java.util.ArrayList;

/**
 * Created by bruno on 25/05/17.
 */

public class AdapterContato extends ArrayAdapter<Contato> {

  private final Context context;
  private final ArrayList<Contato> contatos;

  public AdapterContato(Context context, ArrayList<Contato> contatos) {
    super(context, R.layout.list_item, contatos);
    this.context = context;
    this.contatos = contatos;
  }


  @NonNull
  @Override
  public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    View linha = inflater.inflate(R.layout.list_item, parent, false);

    TextView nome = (TextView) linha.findViewById(R.id.tv_nome);
    TextView email = (TextView) linha.findViewById(R.id.tv_email);
    TextView telefone = (TextView) linha.findViewById(R.id.tv_telefone);

    nome.setText(contatos.get(position).getNome());
    email.setText(contatos.get(position).getEmail());
    telefone.setText(contatos.get(position).getTelefone());

    return linha;
  }
}
