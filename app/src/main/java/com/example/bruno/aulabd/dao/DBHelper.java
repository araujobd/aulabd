package com.example.bruno.aulabd.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by bruno on 25/05/17.
 */

public class DBHelper extends SQLiteOpenHelper {

  private Context context;
  private static DBHelper INSTANCE;

  private static final String DB_NAME = "contatos.db";
  private static final int DB_VERSION = 1;
  private final String tabela_contato =
      "CREATE TABLE contato ( "
          + "id INTEGER PRIMARY KEY AUTOINCREMENT, "
          + "nome TEXT, "
          + "email TEXT, "
          + "telefone TEXT )";

  private DBHelper(Context context) {
    super(context, DB_NAME, null, DB_VERSION);
    this.context = context;
  }

  public static DBHelper getInstance(Context context) {
    if (INSTANCE == null)
      INSTANCE = new DBHelper(context);
    return INSTANCE;
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    db.execSQL(tabela_contato);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int i, int i1) {
    db.execSQL("DROP TABLE IF EXISTS contato");
    onCreate(db);
  }
}
