package com.example.bruno.aulabd;

import android.database.SQLException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.bruno.aulabd.dao.DAO;
import com.example.bruno.aulabd.domain.Contato;

public class CadastroActivity extends AppCompatActivity {

  private EditText ed_nome, ed_email, ed_telefone;
  private Button bt_salvar;
  private DAO dao;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_cadastro);

    setTitle("Novo Contato");

    dao = DAO.getInstance(this);

    configurarTela();
  }

  private void configurarTela() {
    ed_nome = (EditText) findViewById(R.id.ed_nome);
    ed_email = (EditText) findViewById(R.id.ed_email);
    ed_telefone = (EditText) findViewById(R.id.ed_telefone);
    bt_salvar = (Button) findViewById(R.id.bt_salvar);

    bt_salvar.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Contato contato = new Contato(
            ed_nome.getText().toString(),
            ed_email.getText().toString(),
            ed_telefone.getText().toString()
        );

        try {
          dao.salvar(contato);
          finish();
        } catch (SQLException e) {
          Toast.makeText(CadastroActivity.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
      }
    });
  }
}
