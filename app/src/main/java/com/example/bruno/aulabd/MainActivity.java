package com.example.bruno.aulabd;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.example.bruno.aulabd.dao.DAO;
import com.example.bruno.aulabd.domain.AdapterContato;
import com.example.bruno.aulabd.domain.Contato;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements TextWatcher {

  private EditText ed_buscar;
  private ListView listView;
  private AdapterContato adapter;
  private CoordinatorLayout layout;
  private FloatingActionButton fab;
  private ArrayList<Contato> contatos;
  private DAO dao;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    ActionBar toolbar = getSupportActionBar();
    toolbar.setTitle("Contatos");
    layout = (CoordinatorLayout) findViewById(R.id.layout);

    contatos = new ArrayList<>();
    dao = DAO.getInstance(this);
    configurarTela();
    configurarLista();
  }

  @Override
  public void onBackPressed() {
    sair();
  }

  @Override
  protected void onRestart() {
    getLista("");
    super.onRestart();
  }

  private void configurarTela() {
    ed_buscar = (EditText) findViewById(R.id.ed_buscar);
    fab = (FloatingActionButton) findViewById(R.id.fab);

    ed_buscar.addTextChangedListener(this);

    fab.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        startActivity(new Intent(MainActivity.this, CadastroActivity.class));
      }
    });
  }

  public void configurarLista() {
    listView = (ListView) findViewById(R.id.list_view);

    contatos = dao.listar("");
    adapter = new AdapterContato(this, contatos);

    listView.setAdapter(adapter);

    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Contato selecionado = (Contato) adapterView.getItemAtPosition(i);
        Intent intent = new Intent(MainActivity.this, DetalhesActivity.class);
        intent.putExtra("contato", selecionado);
        startActivity(intent);
      }
    });

    listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
      @Override
      public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
//        Contato selecionado = (Contato) adapterView.getItemAtPosition(i);
//        Toast.makeText(MainActivity.this, selecionado.getId() + " || " + selecionado.getNome(), Toast.LENGTH_LONG).show();
        return true;
      }
    });
  }

  private void getLista(String nome) {
    contatos.clear();
    contatos.addAll(dao.listar(nome));

    adapter.notifyDataSetChanged();
  }

  @Override
  public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

  }

  @Override
  public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    getLista(charSequence.toString());
  }

  @Override
  public void afterTextChanged(Editable editable) {
  }

  private void sair() {
    new AlertDialog.Builder(this)
        .setMessage("Deseja realmente sair?")
        .setCancelable(false)
        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialogInterface, int i) {
            MainActivity.this.finish();
          }
        })
        .setNegativeButton("Não", null)
        .show();
  }
}
