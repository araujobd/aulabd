package com.example.bruno.aulabd.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.example.bruno.aulabd.domain.Contato;

import java.util.ArrayList;

/**
 * Created by bruno on 25/05/17.
 */

public class DAO {

  private static DAO INSTANCE;
  private DBHelper dbHelper;
  private SQLiteDatabase db;
  private Context context;

  private DAO(Context context) {
    this.context = context;
    dbHelper = DBHelper.getInstance(context);
  }

  public static DAO getInstance(Context context) {
    if (INSTANCE == null)
      INSTANCE = new DAO(context);
    return INSTANCE;
  }


  public void salvar(Contato contato) throws SQLException {
    abrirDB();
    db.insert("contato", null, contato.getValues());
    Toast.makeText(context, contato.getNome() + " cadastrado!", Toast.LENGTH_LONG).show();
    fecharDB();
  }

  public void atualizar(Contato contato) throws SQLException {
    abrirDB();
    db.update("contato", contato.getValues(), "id = " + contato.getId(), null);

    fecharDB();
  }

  public void remover (Contato contato) throws SQLException {
    abrirDB();
    db.delete("contato", "id = " + contato.getId(), null);

    fecharDB();
  }

  public Contato buscar(int id) throws SQLException {
    Contato contato = new Contato();

    db = dbHelper.getReadableDatabase();
    Cursor cursor = db.rawQuery("SELECT * FROM contato WHERE id = " + id, null);

    if(cursor != null && cursor.moveToFirst()) {
      contato.setId(cursor.getInt(cursor.getColumnIndex("id")));
      contato.setNome(cursor.getString(cursor.getColumnIndex("nome")));
      contato.setEmail(cursor.getString(cursor.getColumnIndex("email")));
      contato.setTelefone(cursor.getString(cursor.getColumnIndex("telefone")));
    }

    cursor.close();
    fecharDB();

    return contato;
  }

  public ArrayList<Contato> listar(String nome) {
    ArrayList<Contato> contatos = new ArrayList<>();

    db = dbHelper.getReadableDatabase();
    Cursor cursor;
    if (nome.length() == 0)
      cursor = db.rawQuery("SELECT * FROM contato", null);
    else
      cursor = db.rawQuery("SELECT * FROM contato WHERE nome like '%" + nome + "%'", null);

    if(cursor.getCount() > 0) {
      cursor.moveToFirst();

      do {
        Contato contato = new Contato(
            cursor.getInt((cursor.getColumnIndex("id"))),
            cursor.getString(cursor.getColumnIndex("nome")),
            cursor.getString(cursor.getColumnIndex("email")),
            cursor.getString(cursor.getColumnIndex("telefone"))
        );

        contatos.add(contato);
      } while (cursor.moveToNext());
    }
    cursor.close();
    fecharDB();

    return contatos;
  }


  private  void abrirDB() throws SQLException{
    if (db == null || !db.isOpen())
      db = dbHelper.getWritableDatabase();
  }

  private void fecharDB() throws SQLException {
    if(db != null && db.isOpen())
      db.close();
  }

}
