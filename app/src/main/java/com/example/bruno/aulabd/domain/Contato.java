package com.example.bruno.aulabd.domain;

import android.content.ContentValues;

import java.io.Serializable;

/**
 * Created by bruno on 25/05/17.
 */

public class Contato implements Serializable {

  private int id;
  private String nome;
  private String email;
  private String telefone;

  public Contato() {

  }

  public Contato(int id, String nome, String email, String telefone) {
    this.id = id;
    this.nome = nome;
    this.email = email;
    this.telefone = telefone;
  }

  public Contato(String nome, String email, String telefone) {
    this.nome = nome;
    this.email = email;
    this.telefone = telefone;
  }
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getTelefone() {
    return telefone;
  }

  public void setTelefone(String telefone) {
    this.telefone = telefone;
  }

  public ContentValues getValues() {
    ContentValues values = new ContentValues();

    values.put("nome", this.nome);
    values.put("email", this.email);
    values.put("telefone", this.telefone);

    return values;
  }

  @Override
  public String toString() {
    return this.nome;
  }
}
