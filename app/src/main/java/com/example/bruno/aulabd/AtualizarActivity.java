package com.example.bruno.aulabd;

import android.content.Intent;
import android.database.SQLException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.bruno.aulabd.dao.DAO;
import com.example.bruno.aulabd.domain.Contato;

public class AtualizarActivity extends AppCompatActivity {

  private EditText ed_nome, ed_email, ed_telefone;
  private Button bt_salvar;

  private Contato contato;
  private DAO dao;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_atualizar);

    setTitle("Atualizar Contato");

    dao = DAO.getInstance(this);

    configurarTela();
    recuperarDados();
  }

  private void recuperarDados() {
    Intent intent = getIntent();
    this.contato = (Contato) intent.getSerializableExtra("contato");

    ed_nome.setText(contato.getNome());
    ed_email.setText(contato.getEmail());
    ed_telefone.setText(contato.getTelefone());
  }

  private void configurarTela() {
    ed_nome = (EditText) findViewById(R.id.ed_nome);
    ed_email = (EditText) findViewById(R.id.ed_email);
    ed_telefone = (EditText) findViewById(R.id.ed_telefone);

    bt_salvar = (Button) findViewById(R.id.bt_salvar);

    bt_salvar.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        contato.setNome(ed_nome.getText().toString());
        contato.setEmail(ed_email.getText().toString());
        contato.setTelefone(ed_telefone.getText().toString());

        try {
          dao.atualizar(contato);
          finish();
        } catch (SQLException e) {
          Toast.makeText(AtualizarActivity.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
      }
    });
  }

}
